const getters = {
  routes: (state) => state.system.routes,
  menus: (state) => state.system.menus,
  unBind: (state) => state.files.unBindFiles,
};

export default getters;
