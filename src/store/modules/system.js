export default {
  namespaced: true,
  state: {
    routes: [],
    menus: [],
  },
  mutations: {
    SET_ROUTES(state, routes) {
      state.routes = routes;
    },
    SET_Menu(state, menus) {
      state.menus = menus;
    },
  },
};
