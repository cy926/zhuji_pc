export default {
  namespaced: true,
  state: {
    unBindFiles: [],
    currentID: 0,
  },
  mutations: {
    SET_UNBIND(state, files) {
      state.unBindFiles.push(files);
    },
    REMOVE_UNBIND(state, files) {
      let dfid = 0;
      if (files.status == "success") {
        dfid = files.response.data.annex_id;
      }
      state.unBindFiles = state.unBindFiles.filter((i) => {
        return i.annex_id != dfid;
      });
    },
    SET_CURRENT(state, id) {
      state.currentID = id;
    },
    UNBIND_RESET(state) {
      state.unBindFiles = [];
    },
  },
};
