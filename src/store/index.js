import Vue from "vue";
import Vuex from "vuex";
import system from "./modules/system";
import files from "./modules/files";
import getters from "./getter";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    system,
    files,
  },
  getters,
});
