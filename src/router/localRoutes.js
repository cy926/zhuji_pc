const localRoutes = [
  //
  {
    menu_type: 2,
    path: "instructionsDetails/:id/:type",
    name: "InstructionsDetails",
    Component: "views/LeaderInstructions/Details",
  },
  {
    menu_type: 2,
    path: "feedbackDetails/:id",
    name: "FeedbackDetails",
    Component: "views/LeaderInstructions/FeedbackDetails",
  },
  {
    menu_type: 2,
    path: "evaluationDetails/:id/:assignId",
    name: "EvaluationDetails",
    Component: "views/LeaderInstructions/EvaluationDetails",
  },
  //
  {
    menu_type: 2,
    path: "checkAndRectifyDetails/:id/:type",
    name: "CheckAndRectifyDetails",
    Component: "views/CheckAndRectify/Details",
  },
  {
    menu_type: 2,
    path: "checkAndRectifyFeedbackDetails/:id/:type",
    name: "CheckAndRectifyFeedbackDetails",
    Component: "views/CheckAndRectify/FeedbackDetails",
  },
  {
    menu_type: 2,
    path: "checkAndRectifyEvaluationDetails/:id/:type",
    name: "CheckAndRectifyEvaluationDetails",
    Component: "views/CheckAndRectify/EvaluationDetails",
  },
  //
  {
    menu_type: 2,
    path: "projectQuestionForm/:id/:type",
    name: "ProjectQuestionForm",
    Component: "views/MajorProjectPromotion/QuestionForm",
  },
  {
    menu_type: 2,
    path: "projectQuestionDetails/:id",
    name: "ProjectQuestionDetails",
    Component: "views/MajorProjectPromotion/QuestionDetails",
  },
  {
    menu_type: 2,
    path: "questionFeedbackDetails/:id",
    name: "QuestionFeedbackDetails",
    Component: "views/MajorProjectPromotion/QuestionFeedbackDetails",
  },
  {
    menu_type: 2,
    path: "questionEvaluationDetails/:id",
    name: "QuestionEvaluationDetails",
    Component: "views/MajorProjectPromotion/QuestionEvaluationDetails",
  },
  {
    menu_type: 2,
    path: "commonForm/:targetTable/:id",
    name: "CommonForm",
    Component: "components/GenerateForm/CommonForm",
  },
  {
    menu_type: 2,
    path: "BmPolicySupport/:id",
    name: "BmPolicySupport",
    Component: "views/BackendManage/BmPolicySupport",
  },
  {
    menu_type: 2,
    path: "system/user/details/:id",
    name: "User",
    Component: "views/System/User/Details",
  },
  {
    menu_type: 2,
    path: "system/dept/details/:id",
    name: "Dept",
    Component: "views/System/Dept/Details",
  },
  // 政策支撑
  {
    menu_type: 2,
    path: "PolicyAdd",
    name: "PolicyAdd",
    Component: "views/BackendManage/PolicySupport/PolicyAdd",
  },
  {
    menu_type: 2,
    path: "PolicyAdd/:id",
    name: "PolicyDetail",
    Component: "views/BackendManage/PolicySupport/PolicyAdd",
  },
  // {
  //   menu_type: 2,
  //   path: "PolicySupport/PolicyDetails",
  //   name: "PolicyDetails",
  //   Component: "views/BackendManage/PolicySupport/PolicyDetails",
  // },
  {
    menu_type: 2,
    path: "system/role/details/:id",
    name: "Role",
    Component: "views/System/Role/Details",
  },
  {
    menu_type: 2,
    path: "majorProjectPromotion/progressDetails/:id/:year/:month/:type",
    name: "ProgressDetails",
    Component: "views/MajorProjectPromotion/ProgressDetails",
  },
  {
    menu_type: 2,
    path: "redAlert/assignForm/:id/:type",
    name: "RedAlertAssignForm",
    Component: "views/RedAlert/AlertAssignForm",
  },
  {
    menu_type: 2,
    path: "redAlert/details/:id/:type",
    name: "RedAlertDetails",
    Component: "views/RedAlert/AlertDetails",
  },
  {
    menu_type: 2,
    path: "alertRectification/details/:id/:type",
    name: "AlertRectification",
    Component: "views/AlertRectification/Details",
  },
  {
    menu_type: 2,
    path: "problemTransfer/details/:id/:type",
    name: "ProblemDetail",
    Component: "views/ProblemTransfer/ProblemDetail",
  },
  {
    menu_type: 2,
    path: "yellowAlert/details/:id",
    name: "YellowAlertDetails",
    Component: "views/YellowAlert/Details",
  },
  {
    menu_type: 2,
    path: "yearplan/declare/:id/:type",
    name: "YearPlanDeclareForm",
    Component: "views/YearPlan/components/DeclareForm",
  },
  {
    menu_type: 2,
    path: "assessmentForm/:id",
    name: "AssessmentForm",
    Component: "views/WorkCollection/components/AssessmentForm",
  },
  {
    menu_type: 2,
    path: "assessmentDetail/:id/:pageType",
    name: "AssessmentDetail",
    Component: "views/WorkCollection/components/AssessmentDetail",
  },
  {
    menu_type: 2,
    path: "settlementForm/:id",
    name: "SettlementForm",
    Component: "views/WorkCollection/components/SettlementForm",
  },
  {
    menu_type: 2,
    path: "tenderControlForm/:id",
    name: "TenderControlForm",
    Component: "views/WorkCollection/components/TenderControlForm",
  },
  {
    menu_type: 2,
    path: "auditDetails/:id/:pageType",
    name: "AuditDetails",
    Component: "views/Audit/AuditDetail",
  },
  // 审计问题详情页
  {
    menu_type: 2,
    path: "auditQuestion/:id",
    name: "AuditQuestion",
    Component: "views/Audit/AuditQuestion",
  },
  {
    menu_type: 2,
    path: "auditQuestionFeedback/:id",
    name: "AuditQuestionFeedback",
    Component: "views/Audit/AuditQuestionFeedback",
  },
  {
    menu_type: 2,
    path: "auditQuestionReview/:id",
    name: "AuditQuestionReview",
    Component: "views/Audit/AuditQuestionReview",
  },
  {
    menu_type: 2,
    path: "mine",
    name: "Mine",
    Component: "views/System/Mine/Index",
  },
];

export default localRoutes;
