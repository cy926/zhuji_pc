import { getDeptTree } from "@/api/System/Department";

export default {
  data() {
    return {
      dept: [],
      isAdmin: JSON.parse(localStorage.getItem("user")).is_admin || 0,
    };
  },
  methods: {
    initDept() {
      getDeptTree().then((res) => {
        if (res.code == 0) {
          this.dept = res.data;
        }
      });
    },
    changeDept(val) {
      this.queryParams.responsible_unit_id = val.value;
    },
  },
  created() {
    this.initDept();
  },
};
