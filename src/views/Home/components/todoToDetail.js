const columns = [
  {
    prop: "type",
    label: "待办类型",
    width: "200",
    minWidth: "",
    align: "",
  },
  {
    prop: "object_name",
    label: "待办对象",
    width: "",
    minWidth: "200",
    align: "",
  },
  {
    prop: "assign_status_text",
    label: "状态",
    width: "200",
    minWidth: "",
    align: "",
  },
];

export default {
  data() {
    return { columns: columns };
  },
  methods: {},
  watch: {
    todoList() {
      this.$nextTick(() => {
        this.$refs.table.doLayout();
      });
    },
  },
};
