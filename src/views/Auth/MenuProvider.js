import router from "@/router/index";
export default {
  methods: {
    generateRoute(routeData) {
      console.log(routeData);
      console.log(router);
      router.addRoute({
        name: "Index",
        path: "/",
        component: () => import("@/components/layout/Index.vue"),
        children: transformRouter(routeData),
      });
    },
  },
};

function transformRouter(list) {
  return list.map((item) => {
    if (item.children) transformRouter(item.children);

    if (item.LinkUrl != undefined) {
      item.name = item.LinkUrl;
      item.path = item.LinkUrl;
    }
    // if (item.Component && item.Type == 2) {
    if (item.Component) {
      item.component = () => import(`@/${item.Component}.vue`);
    } else {
      item.component = { render: (e) => e("router-view") };
    }
    return item;
  });
}
