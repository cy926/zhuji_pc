export default {
  methods: {
    typeCombo(data) {
      if (!data) {
        return;
      }

      if (data.length == 0) {
        return;
      }

      return data.map((i) => {
        let typeArr = [];
        if (i.problem_type_safety && i.problem_type_safety == 1) {
          typeArr.push("工程安全");
        }
        if (i.problem_type_quality && i.problem_type_quality == 1) {
          typeArr.push("工程质量");
        }
        if (i.problem_type_build && i.problem_type_build == 1) {
          typeArr.push("文明施工");
        }
        if (i.problem_type_duty && i.problem_type_duty == 1) {
          typeArr.push("人员到岗");
        }

        i.problem_type = typeArr.join(",");

        return i;
      });
    },
  },
};
