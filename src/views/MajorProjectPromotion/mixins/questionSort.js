import { postQuestionReview } from "@/api/MajorProjectPromotion/Index";

export default {
  data() {
    return {
      problem_level: 0,
    };
  },
  methods: {
    handleQuestionSort() {
      postQuestionReview({
        problem_level: this.problem_level,
        id: this.form.id,
      }).then((res) => {
        if (res.code == 0) {
          this.$message.success("问题梳理成功");
          this.dialog.questionSort = false;
          this.linkTo(`/majorProjectPromotionQuestionReview`);
        }
      });
    },
  },
};
