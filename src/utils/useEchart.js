import * as echarts from "echarts/core";
// import "echarts-gl";
import Vue from "vue";

import {
  BarChart,
  PieChart,
  LineChart,
  SunburstChart,
  FunnelChart,
} from "echarts/charts";
import {
  TitleComponent,
  TooltipComponent,
  GridComponent,
  LegendComponent,
  ToolboxComponent,
} from "echarts/components";
import { CanvasRenderer } from "echarts/renderers";

// 注册必须的组件
echarts.use([
  TitleComponent,
  TooltipComponent,
  ToolboxComponent,
  GridComponent,
  BarChart,
  SunburstChart,
  CanvasRenderer,
  PieChart,
  LineChart,
  LegendComponent,
  FunnelChart,
]);

Vue.prototype.$echarts = echarts;
