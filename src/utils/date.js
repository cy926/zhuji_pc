/**
 * 获取当前日期 yyyy-MM-dd
 * @returns 
 */
let getCurrentDate = () => {
  let date = new Date()

  return date.getFullYear() + "-" + zeroCheck(date.getMonth() + 1) + "-" + zeroCheck(date.getDate())
}

/**
 * 获取当前日期 yyyy-MM-dd HH:mm:ss
 * @returns 
 */
let getCurrentDateTime = () => {
  let date = new Date()

  return date.getFullYear() + "-" +
    zeroCheck(date.getMonth() + 1) + "-" +
    zeroCheck(date.getDate()) + " " +
    zeroCheck(date.getHours()) + ":" +
    zeroCheck(date.getMinutes()) + ":" +
    zeroCheck(date.getSeconds())

}
function zeroCheck(i) {
  if (i >= 0 && i < 10) {
    return "0" + i
  } else {
    return i
  }
}

export { getCurrentDate, getCurrentDateTime }