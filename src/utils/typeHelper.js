/**
 * 问题整改 问题来源
 * @param {*} val
 * @returns
 */
export const questionSource = (val) => {
  switch (parseInt(val)) {
    case 1:
      return "巡查检查";
    case 2:
      return "单位内审";
    case 3:
      return "审计问题";
    default:
      return "";
  }
};

/**
 * 问题整改详情跳转 详情页地址
 * @param {*} val item info
 * @param {*} pageType 列表进度 0 登记 1 反馈 2 评价
 */
export const questionToDetail = (val, pageType) => {
  switch (parseInt(val.row.problem_source)) {
    case 3:
      if (pageType == 0) {
        return `/auditQuestion/${val.row.id}`;
      }
      if (pageType == 1) {
        return `/auditQuestionFeedback/${val.row.id}`;
      }
      if (pageType == 2) {
        return `/auditQuestionReview/${val.row.id}`;
      }
    default:
      if (pageType == 0) {
        return `/checkAndRectifyDetails/${val.row.id}/${pageType}`;
      }
      if (pageType == 1) {
        return `/checkAndRectifyFeedbackDetails/${val.row.id}/${pageType}`;
      }
      if (pageType == 2) {
        return `/checkAndRectifyEvaluationDetails/${val.row.id}/${pageType}`;
      }
  }
};

export const questionDetailToList = (val, pageType) => {
  switch (parseInt(val.row.problem_source)) {
    case 3:
      break;
    default:
      if (pageType == 0) {
        return `/checkAndRectifyRegistration`;
      }
      if (pageType == 1) {
        return `/checkAndRectifyFeedback`;
      }
      if (pageType == 2) {
        return `/checkAndRectifyEvaluation`;
      }
  }
  h;
};
