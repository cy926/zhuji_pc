export const required = {
  required: true,
  message: "不能为空",
};

export const number = {
  type: "number",
  message: "只允许数字类型",
};

var fileValidate = (rule, value, callback) => {
  if (value == 0) {
    callback(new Error("请上传必要的附件"));
  }

  if (value > 0) {
    callback();
  }
};

export const haveFile = {
  required: true,
  validator: fileValidate,
  trigger: "change",
};
