const generateform = [
  {
    type: "div",
    // style: { width: "300px" },
    class: ["rows"],
    children: [
      {
        type: "el-input",
        name: "用户名",
        dbField: "Username",
        bindModel: "",
        rules: {},
        disabled: false,
        placeholder: "请输入XXXX",
        clearable: true,
        maxlength: 10,
        style: { width: "600px" },
      },
    ],
  },
  {
    type: "div",
    // style: { width: "300px" },
    class: ["rows"],
    children: [
      {
        type: "el-textarea",
        name: "用户名",
        dbField: "desc",
        bindModel: "",
        rules: {},
        disabled: false,
        placeholder: "请输入XXXX",
        clearable: true,
        minRows: 5,
        maxRows: 6,
        maxlength: 50,
        style: { width: "600px" },
        labelStyle: {
          "align-self": "flex-start",
        },
      },
    ],
  },
  {
    type: "div",
    class: ["rows"],
    children: [
      {
        type: "el-select",
        name: "用户名",
        dbField: "unit",
        bindModel: "",
        rules: {},
        disabled: false,
        placeholder: "请输入XXXX",
        clearable: true,
        options: [
          { label: "发改局", value: 1 },
          { label: "交通运输局", value: 2 },
          { label: "城投集团", value: 3 },
        ],
        style: { width: "200px" },
      },
      {
        type: "el-date-picker",
        name: "申报日期",
        dbField: "createDate",
        bindModel: "",
        rules: {},
        disabled: true,
        placeholder: "请输入日期时间",
        clearable: true,
        dateType: "date",
        valueFormat: "yyyy-MM-dd",
        style: { width: "200px" },
      },
    ],
  },
  {
    type: "div",
    class: ["rows"],
    children: [
      {
        type: "el-input-number",
        name: "数字",
        dbField: "count",
        bindModel: "",
        rules: {},
        placeholder: "数字",
        disabled: false,
        // 控制器位置
        controlsPosition: "right",
        // 最小值
        min: 0,
        // 最大值
        max: 9999,
        // 精度
        precision: 2,
      },
    ],
  },
  {
    type: "div",
    class: ["rows"],
    children: [
      {
        type: "el-cascader",
        name: "部门列表",
        dbField: "unit",
        bindModel: "",
        rules: {},
        placeholder: "级联选择",
        disabled: false,
        clearable: true,
        filterable: true,
        options: [
          {
            label: "发改局",
            value: 1,
            children: [
              {
                value: "yizhi",
                label: "一致",
              },
              {
                value: "fankui",
                label: "反馈",
              },
              {
                value: "xiaolv",
                label: "效率",
              },
              {
                value: "kekong",
                label: "可控",
              },
            ],
          },
          { label: "交通运输局", value: 2 },
          { label: "城投集团", value: 3 },
        ],
        multiple: false,
        // style: { width: "200px" },
      },
    ],
  },
  {
    type: "div",
    class: ["rows"],
    children: [
      {
        type: "el-upload",
        name: "测试图片",
        dbField: "unit",
        bindModel: "",
        rules: {},
        disabled: false,
        clearable: true,
        filterable: true,
        tableName: "test",
        useType: "img",
      },
    ],
  },
];

export default generateform;
