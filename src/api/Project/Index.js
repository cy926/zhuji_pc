import { get, post } from "@/api/request";

const apiUrl = {
  list: "/project/selectProjectList",
};

/**
 * 获取所有项目列表
 * @returns
 */
export const getProjectList = (data) => {
  return get(apiUrl.list, data);
};
