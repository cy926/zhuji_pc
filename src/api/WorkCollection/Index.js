import { get, post } from "@/api/request";

const apiUrl = {
  frontNode: "/provincialPlatform/getList",
  projectChangesList: "/projectChange/projectChangeList",
  projectContractList: "/projectContract/projectContractList",
  completionAcceptanceList: "/projectAccept/projectAcceptList",
  managerChangeList: "/personChange/projectChangeList",
  bidList: "/bid/bidList",
  securityIncidentList: "/accident/accidentList",

  performanceSelfAssessmentList: "/appraise/appraiseList",
  performanceSelfAssessmentDetail: "/appraise/appraiseDetail",
  performanceSelfAssessmentDoneConfirm: "/appraise/evaluate",
  performanceSelfAssessmentReview: "/appraise/examine",
  performanceSelfAssessmentProjectList: "/appraise/projectList",
  performanceSelfAssessmentSave: "/appraise/saveAppraise",

  settlementList: "/settlement/settlementList",
  settlementBidList: "/settlement/bidList",
  settlementProjectList: "/settlement/projectList",
  settlementSave: "/settlement/saveUpdate",
  settlementDetail: "/settlement/settlementDetail",
  settlementDelete: "/settlement/deleteSettlement",

  bidControlList: "/bidControl/bidControlList",
  bidControlBidList: "/bidControl/bidList",
  bidControlProjectList: "/bidControl/projectList",
  bidControlSave: "/bidControl/saveUpdate",
  bidControlDetail: "/bidControl/bidControlDetail",
  bidControlDelete: "/bidControl/deleteBidControl",
};

export const getFrontNode = (data) => {
  return get(apiUrl.frontNode, data);
};

export const getProjectChangesList = (data) => {
  return get(apiUrl.projectChangesList, data);
};

export const getProjectContractList = (data) => {
  return get(apiUrl.projectContractList, data);
};

export const getCompletionAcceptanceList = (data) => {
  return get(apiUrl.completionAcceptanceList, data);
};

export const getManagerChangeList = (data) => {
  return get(apiUrl.managerChangeList, data);
};

export const getBidList = (data) => {
  return get(apiUrl.bidList, data);
};

export const getSecurityIncidentList = (data) => {
  return get(apiUrl.securityIncidentList, data);
};

export const getPerformanceSelfAssessmentList = (data) => {
  return get(apiUrl.performanceSelfAssessmentList, data);
};

export const getPerformanceSelfAssessmentDetail = (data) => {
  return get(apiUrl.performanceSelfAssessmentDetail, data);
};

export const postPerformanceSelfAssessmentDoneConfirm = (data) => {
  return post(apiUrl.performanceSelfAssessmentDoneConfirm, data);
};

export const postPerformanceSelfAssessmentReview = (data) => {
  return post(apiUrl.performanceSelfAssessmentReview, data);
};

export const getPerformanceSelfAssessmentProjectList = (data) => {
  return get(apiUrl.performanceSelfAssessmentProjectList, data);
};

export const postPerformanceSelfAssessmentSave = (data) => {
  return post(apiUrl.performanceSelfAssessmentSave, data);
};

//
export const getSettlementList = (data) => {
  return get(apiUrl.settlementList, data);
};

export const getSettlementBidList = (data) => {
  return get(apiUrl.settlementBidList, data);
};

export const getSettlementProjectList = (data) => {
  return get(apiUrl.settlementProjectList, data);
};

export const getSettlementDetail = (data) => {
  return get(apiUrl.settlementDetail, data);
};

export const getSettlementDelete = (data) => {
  return get(apiUrl.settlementDelete, data);
};

export const postSettlementSave = (data) => {
  return post(apiUrl.settlementSave, data);
};

//
export const getBidContorlList = (data) => {
  return get(apiUrl.bidControlList, data);
};

export const getBidContorlBidList = (data) => {
  return get(apiUrl.bidControlBidList, data);
};

export const getBidContorlProjectList = (data) => {
  return get(apiUrl.bidControlProjectList, data);
};

export const getBidContorlDetail = (data) => {
  return get(apiUrl.bidControlDetail, data);
};

export const getBidContorlDelete = (data) => {
  return get(apiUrl.bidControlDelete, data);
};

export const postBidContorlSave = (data) => {
  return post(apiUrl.bidControlSave, data);
};
