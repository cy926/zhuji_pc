import { get, post } from "@/api/request";

const apiUrl = {
  progressCondition: "/monthProgress/selectYearAndMonth",
  progressList: "/monthProgress/getProgressList",
  progressReivewList: "/monthProgress/getExamineList",
  progressDetails: "/monthProgress/progressDetil",
  progressSave: "/monthProgress/saveProgress",
  progressReview: "/monthProgress/progressExamine",
  projectQusetionAdd: "/Problem/saveProjectProblem",

  questionList: "/Problem/selectProblemList",
  assignList: "/Problem/selectAssignList",
  evaluationList: "/Problem/selectMendList",

  questionDetails: "/Problem/problemDetail",
  assignDetails: "/Problem/assignDetail",
  evaluationDetails: "/Problem/mendDetail",

  questionAdd: "/Problem/saveProblem", // 项目问题 新增
  questionReivew: "/Problem/checkProblem", // 项目问题 审核
  assignAdd: "/Problem/saveAssign", // 问题交办 新增
  assignFeedback: "/Problem/saveMend", // 交办反馈
  assignEvaluation: "/Problem/updateMend", // 结果评价

  // 进度上报情况
  progressAnalyze: "/monthProgress/ProgressSituation",
  // 进度上报情况列表
  progressAnalyzeList: "/monthProgress/getProgressSituationList",
  dailyStatistics: "/project/dailyStatistics",
};

/**
 * 获取默认搜索条件 年月
 * @param {*} data
 * @returns
 */
export const getProgressCondition = (data) => {
  return get(apiUrl.progressCondition, data);
};

/**
 * 获取进度列表
 * @param {*} data
 * @param {String} project_name 项目名称
 * @param {String} year 年
 * @param {String} month 月
 * @param {String} report_status 上报状态  0 暂存 10 待审核 20 退回 100 通过
 * @returns
 */
export const getProgressList = (data) => {
  return get(apiUrl.progressList, data);
};

/**
 * 获取进度审核列表
 * @param {*} data
 * @param {String} project_name 项目名称
 * @param {String} year 年
 * @param {String} month 月
 * @param {String} report_status 上报状态  0 暂存 10 待审核 20 退回 100 通过
 * @returns
 */
export const getProgressReviewList = (data) => {
  return get(apiUrl.progressReivewList, data);
};

/**
 * 获取进度详情
 * @param {*} data
 * @returns
 */
export const getProgressDetails = (data) => {
  return get(apiUrl.progressDetails, data);
};

/**
 * 进度保存
 * @param {*} data
 * @param {Number} report_status 上报状态 0 暂存 10 上报
 * @param {Number} start_flag 开工状态 0 未开工 1 已开工
 * @param {Number} finish_flag 完工状态 0 未完工 1 已完工
 * @returns
 */
export const postProgressSave = (data) => {
  return post(apiUrl.progressSave, data);
};

/**
 * 进度审核
 * @param {*} data
 * @param {Number} id 进度ID
 * @param {String} opinion 退回原因
 * @param {Number} status 5 退回 100 通过
 * @returns
 */
export const postProgressReview = (data) => {
  return post(apiUrl.progressReview, data);
};

/**
 * 项目问题新增 （进度）
 * @param {*} data
 * @returns
 */
export const postProjectQuestion = (data) => {
  return post(apiUrl.projectQusetionAdd, data);
};

/**
 * 获取项目问题列表
 * @param {*} data
 * @param {int} assign_status 0 未交办 1 已交办 2 已办结
 * @returns
 */
export const getQuestionList = (data) => {
  return get(apiUrl.questionList, data);
};

/**
 * 获取交办单列表
 * @param {*} data
 * @param {int} status 0 未反馈 1 已反馈 2 已退回 3 已办结
 * @returns
 */
export const getAssignList = (data) => {
  return get(apiUrl.assignList, data);
};

/**
 * 获取结果评价列表
 * @param {*} data
 * @param {int} check_status 0 未反馈 1 已反馈 2 已退回 3 已办结
 * @returns
 */
export const getEvaluationList = (data) => {
  return get(apiUrl.evaluationList, data);
};

/**
 * 获取项目问题详情
 * @param {*} data
 * @param {int} id
 * @returns
 */
export const getQuestionDetails = (data) => {
  return get(apiUrl.questionDetails, data);
};

/**
 * 获取交办单详情
 * @param {*} data
 * @param {int} id
 * @returns
 */
export const getAssignDetails = (data) => {
  return get(apiUrl.assignDetails, data);
};

/**
 * 获取评价详情
 * @param {*} data
 * @param {int} id
 * @returns
 */
export const getEvaluationDetails = (data) => {
  return get(apiUrl.evaluationDetails, data);
};

/**
 * 问题交办 新增
 * @param {*} data
 * @returns
 */
export const postAssignSave = (data) => {
  return post(apiUrl.assignAdd, data);
};

/**
 * 项目问题新增
 * @param {int} id 项目问题ID
 * @param {Object} problem_form
 * @returns
 */
export const postQuestionAdd = (data) => {
  return post(apiUrl.questionAdd, data);
};
/**
 * 项目问题审核
 * @param {int} id 项目问题ID
 * @param {int} problem_level 问题级别（0非问题，1一般问题，2交办问题）
 * @returns
 */
export const postQuestionReview = (data) => {
  return post(apiUrl.questionReivew, data);
};

/**
 * 交办反馈
 * @returns
 */
export const postAssignFeedback = (data) => {
  return post(apiUrl.assignFeedback, data);
};

/**
 * 结果评价
 * @returns
 */
export const postEvaluation = (data) => {
  return post(apiUrl.assignEvaluation, data);
};

/**
 * 进度上报情况
 * @returns
 */
export const getProgressAnalyze = (data) => {
  return get(apiUrl.progressAnalyze, data);
};

/**
 * 进度上报情况
 * @returns
 */
export const getProgressAnalyzeList = (data) => {
  return get(apiUrl.progressAnalyzeList, data);
};

/**
 * 日常工作采集统计
 * @returns
 */
export const dailyStatistics = (data) => {
  return get(apiUrl.dailyStatistics, data);
};
