import { get, post } from "@/api/request";

const apiUrl = {
  redAlertList: "/redWarning/redWarningList",
  redAlertAssignList: "/redWarning/managementList",
  resAlertAssign: "/redWarning/sendAssign",
  redAlertDetail: "/redWarning/warningDetail",
  undertakeDept: "/redWarning/assignDeptList",

  redAlertFeedback: "/redWarning/managementAssign",
  redAlertPunish: "/redWarning/managementWarning",
  oneCaseThreeCheck: "/redWarning/warningCheck",
  questionTransfer: "/redWarning/transfer",
  sendRectification: "/mend/sendMend",

  oneCaseThreeCheckList: "/redWarning/warningCheckList",

  assignWithDrawal: "/redWarning/withdraw",

  rectificationDept: "/mend/mendDeptList",

  analyze: "/redWarning/warningStatistics",
};

export const getRedAlertList = (data) => {
  return get(apiUrl.redAlertList, data);
};

export const getRedAlerAssignList = (data) => {
  return get(apiUrl.redAlertAssignList, data);
};

export const postRedAlerAssignDetails = (data) => {
  return post(apiUrl.resAlertAssign, data);
};

export const getRedAlerAssignDetail = (data) => {
  return get(apiUrl.redAlertDetail, data);
};

export const getUndertakeDept = (data) => {
  return get(apiUrl.undertakeDept, data);
};

export const postRedAlertFeedback = (data) => {
  return post(apiUrl.redAlertFeedback, data);
};

export const postRedAlertPunish = (data) => {
  return post(apiUrl.redAlertPunish, data);
};

export const postOneCaseThreeCheck = (data) => {
  return post(apiUrl.oneCaseThreeCheck, data);
};

export const postQuestionTransfer = (data) => {
  return post(apiUrl.questionTransfer, data);
};

export const postSendRectification = (data) => {
  return post(apiUrl.sendRectification, data);
};

export const getOneCaseThreeCheckList = (data) => {
  return get(apiUrl.oneCaseThreeCheckList, data);
};

export const postAssignWithDrawal = (data) => {
  return get(apiUrl.assignWithDrawal, data);
};

export const getRectificationDept = (data) => {
  return get(apiUrl.rectificationDept, data);
};

export const getAnalyze = (data) => {
  return get(apiUrl.analyze, data);
};
