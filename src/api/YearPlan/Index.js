import { get, post } from "@/api/request";

const apiUrl = {
  yearPlanList: "/yearlyPlan/selectProjectList",
  yearPlanDetails: "/yearlyPlan/projectDetail",
  yearPlanSave: "/yearlyPlan/saveProject",

  historyYearPlanList: "/yearlyPlan/maxPlanList",

  yearPlanFirstReviewList: "/yearlyPlan/selectFastTrialList",

  yearPlanFirstReview: "/yearlyPlan/fastTrial",

  industry: "/yearlyPlan/dictList",

  yearPlanReleaseList: "/yearlyPlan/selectReleaseList",

  setYearPlanRelease: "/yearlyPlan/planRelease",
};

/**
 * 获取行业分类
 * @returns
 */
export const getIndustry = (data) => {
  return get(apiUrl.industry, data);
};

/**
 * 年度计划列表
 * @returns
 */
export const getYearPlanList = (data) => {
  return get(apiUrl.yearPlanList, data);
};

/**
 * 年度计划列表 （往年）
 * @returns
 */
export const getHisyoryYearPlanList = (data) => {
  return get(apiUrl.historyYearPlanList, data);
};

/**
 * 年度计划详情
 * @returns
 */
export const getYearPlanDetails = (data) => {
  return get(apiUrl.yearPlanDetails, data);
};

/**
 * 年度计划保存
 * @returns
 */
export const postYearPlanSave = (data) => {
  return post(apiUrl.yearPlanSave, data);
};

/**
 * 年度计划初审列表
 * @returns
 */
export const getYearPlanFirstReviewList = (data) => {
  return get(apiUrl.yearPlanFirstReviewList, data);
};

/**
 * 年度计划初审
 * @returns
 */
export const postYearPlanReview = (data) => {
  return post(apiUrl.yearPlanFirstReview, data);
};

/**
 * 年度计划下达列表（ 已下达 ）
 * @returns
 */
export const getYearPlanReleaseList = (data) => {
  return get(apiUrl.yearPlanReleaseList, data);
};

/**
 * 年度计划下达列表
 * @returns
 */
export const postSetYearPlanRelease = (data) => {
  return post(apiUrl.setYearPlanRelease, data);
};
