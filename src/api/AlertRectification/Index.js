import { get, post } from "@/api/request";

const apiUrl = {
  alertRectificationList: "/mend/warningMendList",
  alertRectificationReviewList: "/mend/warningCheckList",
  alertRectificationDetail: "/mend/warningMendDetail",
  alertRectificationFeedback: "/mend/feedbackMend",
  alertRectificationReview: "/mend/checkfeedbackMend",
};

/**
 * 预警整改列表
 * @param {*} data status 0：未反馈 1：已反馈 2：已审核
 * @returns
 */
export const getAlertRectificationList = (data) => {
  return get(apiUrl.alertRectificationList, data);
};

/**
 * 预警整改列表 审核
 * @param {*} data status 0：未反馈 1：已反馈 2：已审核
 * @returns
 */
export const getAlertRectificationReviewList = (data) => {
  return get(apiUrl.alertRectificationReviewList, data);
};

/**
 * 预警整改详情
 * @param {*} data id
 * @returns
 */
export const getAlertRectificationDetail = (data) => {
  return get(apiUrl.alertRectificationDetail, data);
};

/**
 * 预警整改反馈
 * @param {*} data
 * @returns
 */
export const postAlertRectificationFeedback = (data) => {
  return post(apiUrl.alertRectificationFeedback, data);
};

/**
 * 预警整改审核
 * @param {*} data
 * @returns
 */
export const postAlertRectificationReview = (data) => {
  return post(apiUrl.alertRectificationReview, data);
};
