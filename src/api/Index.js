import { get, post } from "./request";

const apiUrl = {
  login: "/user/login",
  scanDDLogin: "/user/scan/getUserInfo",
  logout: "/user/logout",
  menuList: "/sys-menu/list",
  // get form field
  tableField: "/sys/getFieldList",
  // get select options
  selectOptions: "/sys/getOptions",
  // common save
  commonSave: "/sys/saveUpdateForm",
  commonDetails: "/sys/getDetail",
  
};

/**
 * 用户登录
 * @param {*} data { usercode:String, password:String }
 * @returns
 */
export const login = (data) => {
  return post(apiUrl.login, data);
};

/**
 * 浙政顶扫码登录
 * @param {*} data { code }
 * @returns
 */
export const scanDDLogin = (data) => {
  return get(apiUrl.scanDDLogin, data);
};

/**
 * 用户退出
 * @param {*} data
 * @returns
 */
export const logout = (data) => {
  return post(apiUrl.logout, data);
};

/**
 * 获取表字段
 * @param {*} data
 * @returns
 */
export const getTableField = (data) => {
  return get(apiUrl.tableField, data);
};

/**
 * 公共详情查询
 * @param {*} data
 * @param {string} tableName
 * @param {int} id
 * @returns
 */
export const commonDetails = (data) => {
  return get(apiUrl.commonDetails, data);
};

/**
 * 获取下拉列表选项
 * @param {*} data
 * @param {string} parameter
 * @returns
 */
export const getSelectOptions = (data) => {
  return get(apiUrl.selectOptions, data);
};

/**
 * 公共表单保存
 * @param {*} data
 * @param {object} gData
 * @returns
 */
export const commonSave = (data) => {
  return post(apiUrl.commonSave, data);
};

/**
 * 获取菜单
 * @returns
 */
export const getMenu = (data) => {
  return post(apiUrl.menuList, data);
};
