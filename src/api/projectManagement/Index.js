import { get, post } from '@/api/request';

const apiUrl = {
  bidExternalList: '/bid/bidExternalList',
  bidExternalRelation: '/bid/bidExternalRelation',
  bidExternalDetail: '/bid/bidExternalDetail'
}
export const getbidExternalList = (data) => {
  return get(apiUrl.bidExternalList, data)
}
export const getbidExternalRelation = (data) => {
  return post(apiUrl.bidExternalRelation, data)
}

export const getbidExternalDetail = (data) => {
  return get(apiUrl.bidExternalDetail, data)
}