import { get } from "@/api/request";

const apiUrl = {
  statistics: "/statisticsCollect/getLevel",
  deptRank: "/statisticsCollect/getDeptRank",
  projectRank: "/statisticsCollect/getProjectRank",
  pointRank: "/statisticsCollect/getSubSceneRank",
  deptWarningRank: "/statisticsCollect/getRank",
  summaryByOne: "/statisticsCollect/getLevel",
};

/**
 * 预计灯统计
 * @returns
 */
export const statistics = (data) => {
  return get(apiUrl.statistics, data);
};

/**
 * 部门预警排名
 * @returns
 */
export const getDeptRank = (data) => {
  return get(apiUrl.deptRank, data);
};

/**
 * 项目预警排名
 * @returns
 */
export const getProjectRank = (data) => {
  return get(apiUrl.projectRank, data);
};

/**
 * 节点预计排名排名
 * @returns
 */
export const getPointRank = (data) => {
  return get(apiUrl.pointRank, data);
};

/**
 * 部门预计排名 图表
 * @returns
 */
export const getDeptWarningRank = (data) => {
  return get(apiUrl.deptWarningRank, data);
};

/**
 * 统计
 * @returns
 */
export const getSummaryByOne = (data) => {
  return get(apiUrl.summaryByOne, data);
};
