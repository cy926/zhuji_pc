import { get, post } from "@/api/request";

const apiUrl = {
  instructionsList: "/LeaderInstruct/selectLeaderInstructList",
  feedbackList: "/LeaderInstruct/selectAssignList",
  evaluationList: "/LeaderInstruct/selectMendList",

  instructionsAssignAdd: "/LeaderInstruct/saveAssign",
  instructionsFeedbackAdd: "/LeaderInstruct/saveMend",
  instructionsEvaluationAdd: "/LeaderInstruct/updateMend",

  instructionsDetails: "/LeaderInstruct/LeaderInstructDetail",
  assignDetails: "/LeaderInstruct/assignDetail",

  assignFinish: "/LeaderInstruct/leaderSettle",
};

/**
 * 获取 领导批示列表
 * @param {*} data
 * @param {Number} pageNum
 * @param {Number} pageSize
 * @param {Number} assign_status 0 未交办 1 已交办(未反馈) 2 已交办(已反馈) 3 已交办(已办结)
 * @returns
 */
export const getInstructionsList = (data) => {
  return get(apiUrl.instructionsList, data);
};

/**
 * 获取 批示反馈列表
 * @param {*} data
 * @param {Number} pageNum
 * @param {Number} pageSize
 * @param {Number} assign_status 0 未反馈 1 已反馈 2 已退回 3 已办结
 * @returns
 */
export const getFeedbackList = (data) => {
  return get(apiUrl.feedbackList, data);
};

/**
 * 获取 结果评价列表
 * @param {*} data
 * @param {Number} pageNum
 * @param {Number} pageSize
 * @param {Number} status 0 未评价 1 已评价
 * @returns
 */
export const getEvaluationList = (data) => {
  return get(apiUrl.evaluationList, data);
};

/**
 * 获取 领导批示详情
 * @param {*} data
 * @param {Number} id
 * @returns
 */
export const getInstructionsDetails = (data) => {
  return get(apiUrl.instructionsDetails, data);
};

/**
 * 获取 交办单详情
 * @param {*} data
 * @param {Number} id
 * @returns
 */
export const getAssignDetails = (data) => {
  return get(apiUrl.assignDetails, data);
};

/**
 * 新增交办单
 * @param {*} data
 * @param {Number} id
 * @returns
 */
export const addInstructionsAssign = (data) => {
  return post(apiUrl.instructionsAssignAdd, data);
};

/**
 * 新增反馈
 * @param {*} data
 * @param {Number} id
 * @returns
 */
export const addInstructionsFeedback = (data) => {
  return post(apiUrl.instructionsFeedbackAdd, data);
};

/**
 * 新增评价
 * @param {*} data
 * @param {Number} id
 * @returns
 */
export const addInstructionsEvaluation = (data) => {
  return post(apiUrl.instructionsEvaluationAdd, data);
};

/**
 * 交办单办结确认
 * @param {*} data
 * @param {Number} id
 * @returns
 */
export const postAssignFinish = (data) => {
  return get(apiUrl.assignFinish, data);
};
