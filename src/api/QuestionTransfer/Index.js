import { get, post } from "@/api/request";

const apiUrl = {
  transferList: "/transfer/warningTransferList",
  transferPunishList: "/transfer/managementTransferList",
  transferDetail: "/transfer/ProblemAssignDetail",
  transferAccept: "/transfer/receive",
  transferAssign: "/transfer/sendProblemAssign",
  transferFeedback: "/transfer/receiveProblemAssign",
  transferPunish: "/transfer/managementProblemAssign",
};

/**
 * 获取问题移交列表
 * @param {*} data
 * @returns
 */
export const getTransferList = (data) => {
  return get(apiUrl.transferList, data);
};

/**
 * 获取问题移交处置列表
 * @param {*} data
 * @returns
 */
export const getTransferPunishList = (data) => {
  return get(apiUrl.transferPunishList, data);
};

/**
 * 获取问题移交详情
 * @param {*} data id
 * @returns
 */
export const getTransferDetail = (data) => {
  return get(apiUrl.transferDetail, data);
};

/**
 * 移交问题接收
 * @param {*} data
 * @returns
 */
export const postTransferAccept = (data) => {
  return post(apiUrl.transferAccept, data);
};

/**
 * 移交问题交办
 * @param {*} data
 * @returns
 */
export const postTransferAssign = (data) => {
  return post(apiUrl.transferAssign, data);
};

/**
 * 移交问题反馈
 * @param {*} data
 * @returns
 */
export const postTransferFeedback = (data) => {
  return post(apiUrl.transferFeedback, data);
};

/**
 * 移交问题处置
 * @param {*} data
 * @returns
 */
export const postTransferPunish = (data) => {
  return post(apiUrl.transferPunish, data);
};
