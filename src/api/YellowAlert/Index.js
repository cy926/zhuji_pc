import { get, post } from "@/api/request";

const apiUrl = {
  yellowAlertList: "/yellowWarning/yellowWarning",
  yellowAlertDetail: "/yellowWarning/warningDetail",
  yellowAlertOperate: "/yellowWarning/managementWarning",
  yellowAlertPunish: "/yellowWarning/registerManagement",
  yellowAlertOneCaseThreeCheck: "/yellowWarning/warningCheck",
  yellowAlertNoticeFeedback: "/yellowWarning/followFeedback",
};

/**
 * 获取 黄色预警列表
 * @returns
 */
export const getYellowAlertList = (data) => {
  return get(apiUrl.yellowAlertList, data);
};

/**
 * 获取 黄色预警详情
 * @returns
 */
export const getYellowAlertDetail = (data) => {
  return get(apiUrl.yellowAlertDetail, data);
};

/**
 * 预警处置 操作
 * @param {*} id 预警id
 * @param {*} yellow_follow_status 0 未关注 1 关注
 * @param {*} yellow_know_status 0 未了解 1 了解
 * @returns
 */
export const postYellowAlertOperate = (data) => {
  return post(apiUrl.yellowAlertOperate, data);
};

/**
 * 预警处置 录入
 * @param {*} id 预警id
 * @param {*} yellow_solve_status 黄色预警处置状态， 0：未处置，10：已了解，20：已关注，30：已督办，40：已查处
 * @returns
 */
export const postYellowAlertPunish = (data) => {
  return post(apiUrl.yellowAlertPunish, data);
};

/**
 * 预警处置 一案三查
 * @param {*} id 预警id
 * @param {*} yellow_solve_status 黄色预警处置状态， 0：未处置，10：已了解，20：已关注，30：已督办，40：已查处
 * @returns
 */
export const postYellowAlertOneCaseThreeCheck = (data) => {
  return post(apiUrl.yellowAlertOneCaseThreeCheck, data);
};

/**
 * 关注反馈
 * @param {*} id 预警id
 * @param {*} yellow_feed_back 情况说明
 * @returns
 */
export const postNoticeFeedback = (data) => {
  return post(apiUrl.yellowAlertNoticeFeedback, data);
};
