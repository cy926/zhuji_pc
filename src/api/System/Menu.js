import { get, post } from "@/api/request";

const apiUrl = {
  menuTree: "/sys-menu/allList",
  menuList: "/sys-Role/RoleList",
  menuDelete: "/sys-menu/deleteMenu",
  menuSave: "/sys-menu/saveUpdateMenu",
  roleDetails: "/sys-Role/getRole",
};

export const getMenuTree = (data) => {
  return post(apiUrl.menuTree, data);
};

export const getRoleDetials = (data) => {
  return post(apiUrl.roleDetails, data);
};

export const deleteMenu = (data) => {
  return post(apiUrl.menuDelete, data);
};

export const postMenuSave = (data) => {
  return post(apiUrl.menuSave, data);
};
