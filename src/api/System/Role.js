import { get, post } from "@/api/request";

const apiUrl = {
  roleList: "/sys-Role/RoleList",
  roleDelete: "/sys-Role/deleteRole",
  roleDetails: "/sys-Role/getRole",
  saveRole: "/sys-menu/updateRoleMenu",
};

export const getRoleList = (data) => {
  return post(apiUrl.roleList, data);
};

export const getRoleDetials = (data) => {
  return post(apiUrl.roleDetails, data);
};

export const deleteRole = (data) => {
  return post(apiUrl.roleDelete, data);
};

export const saveRole = (data) => {
  return post(apiUrl.saveRole, data);
};
