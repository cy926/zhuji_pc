import { get, post } from "@/api/request";

const apiUrl = {
  userList: "/sys-User/UserList",
  userDetail: "/sys-User/getUserDetail",
  userSave: "/sys-User/saveUpdateUser",
  userDelete: "/sys-User/deleteUser",
  pwdReset: "/sys-User/InitializePsw",
  getDingAccount: "/app/user/getEmployeeCodeByPhone",

  changePwd: "/sys-User/changePassword",

  titleList: "/sys-User/titleList",

  bsLeaderType: "/sys-User/typeList",
};

/**
 * 获取用户列表
 * @returns
 */
export const getUserList = (data) => {
  return post(apiUrl.userList, data);
};

/**
 * 获取用户详情
 * @returns
 */
export const getUserDetail = (data) => {
  return post(apiUrl.userDetail, data);
};

/**
 * 用户新增编辑
 * @returns
 */
export const postUserSave = (data) => {
  return post(apiUrl.userSave, data);
};

/**
 * 用户删除
 * @returns
 */
export const deleteUser = (data) => {
  return post(apiUrl.userDelete, data);
};

/**
 * 用户 密码重置
 * @returns
 */
export const pwdReset = (data) => {
  return post(apiUrl.pwdReset, data);
};

/**
 * 通过手机号码获取浙政钉用户信息
 * @returns
 */
export const getDingAccount = (data) => {
  return get(apiUrl.getDingAccount, data);
};

/**
 * 修改自己的密码
 * @returns
 */
export const postChangePwd = (data) => {
  return post(apiUrl.changePwd, data);
};

/**
 * 获取头衔列表
 * @returns
 */
export const getTitleList = (data) => {
  return get(apiUrl.titleList, data);
};

/**
 * 获取大屏领导类型
 * @returns
 */
export const getLeaderType = (data) => {
  return get(apiUrl.bsLeaderType, data);
};
