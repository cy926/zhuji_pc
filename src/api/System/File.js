import { get, post } from "@/api/request";

const apiUrl = {
  selectFile: "/sys-file/selectFile",
  bindFile: "/sys-file/bind",
  deleteFile: "/sys-file/deleteFile",
  downloadFile: "/sys-file/download",
  uploadFile: "/sys-file/upload",
};

/**
 * 获取绑定文件
 * @param {*}  data
 * @param {String} tableName
 * @param {String} useType
 * @param {String,Number} pid
 * @returns
 */
export const getFiles = (data) => {
  return post(apiUrl.selectFile, data);
};

/**
 * 绑定文件
 * @param {*} data
 * @param {String} tableName
 * @param {String} useType
 * @param {String,Number} pid
 * @returns
 */
export const postBindFiles = (data) => {
  return post(apiUrl.bindFile, data);
};

/**
 * 删除文件
 * @param {*} data
 * @param {String,Number} id
 * @returns
 */
export const deleteFiles = (data) => {
  return post(apiUrl.deleteFile, data);
};
