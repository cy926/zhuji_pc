import { get, post } from "@/api/request";

const apiUrl = {
  diList: "/sys-supervise/superviseList",
  diEdit: "/sys-supervise/saveUpdate",
  diDelete: "/sys-supervise/delete",
};

export const getDIList = (data) => {
  return get(apiUrl.diList, data);
};

export const postDIInfo = (data) => {
  return post(apiUrl.diEdit, data);
};

export const deleteDI = (data) => {
  return get(apiUrl.diDelete, data);
};
