import { get, post } from "@/api/request";

const apiUrl = {
  permissTree: "/sys-menu/allpermissionList",
};

export const getPermissTree = (data) => {
  return post(apiUrl.permissTree, data);
};
