import { get, post } from "@/api/request";

const apiUrl = {
  deptTree: "/sys-Dept/DeptTree",
  deptList: "/sys-Dept/DeptList",
  deptDelete: "/sys-Dept/deleteDept",
  deptDetails: "/sys-Dept/deptDetail",
  deptSave: "/sys-Dept/saveUpdateDept",
};

/**
 * 获取部门树
 * @returns
 */
export const getDeptTree = (data) => {
  return get(apiUrl.deptTree, data);
};

/**
 * 获取部门列表
 * @returns
 */
export const getDeptList = (data) => {
  return post(apiUrl.deptList, data);
};

/**
 * 删除部门
 * @returns
 */
export const deleteDept = (data) => {
  return post(apiUrl.deptDelete, data);
};

/**
 * 获取部门详情
 * @returns
 */
export const getDeptDetails = (data) => {
  return post(apiUrl.deptDetails, data);
};

/**
 * 保存部门详情
 * @returns
 */
export const postDeptDetails = (data) => {
  return post(apiUrl.deptSave, data);
};
