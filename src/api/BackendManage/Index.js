import { get, post } from "@/api/request";

const apiUrl = {
  setYearPlanLabel: "/yearlyPlan/lable",
  getBMMonthProgress: "/monthProgress/getbackProgressList",
  warningList: "/warningConfig/warningList",

  projectLabelList: "/yearlyPlan/labelList",
  setProjectTag: "/yearlyPlan/labelText",

  setLeader: "/yearlyPlan/leader",

  editWarningNode: "/warningConfig/saveWarning",
  policyList: "/news/newsList",
  policySave: "/news/saveNews",
  policyDetail: "/news/accidentDetail",
  policySupport: '/news/newsPcList'
};

/**
 * 设置项目标签
 * @returns
 */
export const postSetYearPlanLabel = (data) => {
  return post(apiUrl.setYearPlanLabel, data);
};

/**
 * 设置项目标签
 * @returns
 */
export const getBMMonthProgress = (data) => {
  return get(apiUrl.getBMMonthProgress, data);
};

/**
 * 获取预警列表
 * @returns
 */
export const getWarningList = (data) => {
  return get(apiUrl.warningList, data);
};

/**
 * 获取项目标签
 * @returns
 */
export const getTagList = (data) => {
  return get(apiUrl.projectLabelList, data);
};

/**
 * 设置项目标签
 * {  id, lable }
 * @returns
 */
export const setProjectTag = (data) => {
  return post(apiUrl.setProjectTag, data);
};

/**
 * 设置项目标签
 * {  id, leader_id }
 * @returns
 */
export const setProjectLeader = (data) => {
  return post(apiUrl.setLeader, data);
};
/**
 * 预警点修改
 * {  id, leader_id }
 * @returns
 */
export const postEditWarningNode = (data) => {
  return post(apiUrl.editWarningNode, data);
};

// 政策支撑的列表
export const getpolicyList = (data) => {
  return get(apiUrl.policyList, data);
};

// 头部政策支撑列表
export const getpolicySupport = (data) => {
  return get(apiUrl.policySupport, data)
}

// 政策支撑的新增编辑
export const getPolicySave = (data) => {
  return post(apiUrl.policySave, data);
};

// 政策支撑的详情
export const getPolicyDetail = (data) => {
  return get(apiUrl.policyDetail, data);
};

// 政策支撑的删除
export const deletePolicy = (data) => {
  return get(apiUrl.policyDelete, data);
};
