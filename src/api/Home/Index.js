import { get, post } from "@/api/request";

const apiUrl = {
  highFrequencyWarning: "/home/getWarning",
  warningRank: "/cockpit/getDeptRank", // 首页 单位预警总数排名
  statistics: "/home/getStatistics",
  summaryByUnit: "/home/getStatistics2", // 领导批示、问题交办、检查整改旭日图

  dpetStatistics: "/deptHome/getStatistics",
  projectProgress: "/deptHome/getProjectProgress",
  deptWarningPoint: "/deptHome/getProjectWarning",
  deptWarning: "/deptHome/getDeptWarning",
  dpetWarningRank: "/deptHome/getWarning",
  warningStructure: "/deptHome/getWarningStructure",
  deptInvestmentProgress: "/deptHome/getinvestProgress",

  /// 纪委领导首页
  redAlertManage: "/jwHome/getRedList",
  redAlertRank: "/jwHome/getRedRank",
  redAlertDetails: "/jwHome/getRedWarningDetail",
  highFrequencyMatters: "/jwHome/getRedSenceList",
  alertBar: "/jwHome/getRedYellowWarning",
  todoMatters: "/jwHome/gethandleList",
  todoCount: "/jwHome/getWorkHandle",

  /// 纪委组首页
  dcgRedAlertManage: "/jjzHome/getRedList",
  dcgRedAlertRank: "/jjzHome/getRedRank",
  dcgHighFrequencyMatters: "/jjzHome/getRedSenceList",
  dcgRedAlertDetails: "/jjzHome/getRedWarningDetail",
  dcgRedYellowAlert: "/jjzHome/getRedYellowWarning",
  dcgWorkTodo: "/jjzHome/getWorkHandle",
  dcgWorkTodo4: "/jjzHome/gethandleList",

  /// 部门待办
  deptWorkTodo: "/deptHome/gethandleList",
  deptTodoCount: "/deptHome/gethandleStatistics",
};

/**
 * 获取 高频预警排名
 * @returns
 */
export const getHighFrequencyWarning = (data) => {
  return get(apiUrl.highFrequencyWarning, data);
};

// 首页统计 预警单位排名
export const getWarningRank = (data) => {
  return get(apiUrl.warningRank, data);
};

// 首页统计 领导批示、问题交办、检查整改
export const getStatistics = (data) => {
  return get(apiUrl.statistics, data);
};

// 首页统计 领导批示、问题交办、检查整改旭日图
export const getSummaryByUnit = (data) => {
  return get(apiUrl.summaryByUnit, data);
};

// 部门首页  部门统计
export const getDpetStatistics = (data) => {
  return get(apiUrl.dpetStatistics, data);
};

// 部门首页  项目进度
export const getDeptProjectProgress = (data) => {
  return get(apiUrl.projectProgress, data);
};

// 部门首页  本部门预警点 表格
export const getDeptWarningPoint = (data) => {
  return get(apiUrl.deptWarningPoint, data);
};

// 部门首页  本部门预警
export const getDeptWarning = (data) => {
  return get(apiUrl.deptWarning, data);
};

// 部门首页  预警结构
export const getDeptWarningStructure = (data) => {
  return get(apiUrl.warningStructure, data);
};

// 部门首页  本部门排名
export const getDpetWarningRank = (data) => {
  return get(apiUrl.dpetWarningRank, data);
};

// 部门首页  本部门投资进度
export const getDeptInvestmentProgress = (data) => {
  return get(apiUrl.deptInvestmentProgress, data);
};

// ----- 纪委领导首页 -----

// 红色预警管理
export const getRedAlertManage = (data) => {
  return get(apiUrl.redAlertManage, data);
};

// 红色预警排名
export const getRedAlertRank = (data) => {
  return get(apiUrl.redAlertRank, data);
};

// 红色预警明细
export const getRedAlertDetails = (data) => {
  return get(apiUrl.redAlertDetails, data);
};

// 高频事项
export const getHighFrequencyMatters = (data) => {
  return get(apiUrl.highFrequencyMatters, data);
};

// 红黄预警数
export const getAlertBar = (data) => {
  return get(apiUrl.alertBar, data);
};

// 代办事项
export const getTodoMatters = (data) => {
  return get(apiUrl.todoMatters, data);
};

// 代办事项 统计
export const getTodoCount = (data) => {
  return get(apiUrl.todoCount, data);
};

// ----- 纪委组首页 -----

// 红色预警管理
export const getDCGRedAlertManage = (data) => {
  return get(apiUrl.dcgRedAlertManage, data);
};

// 红色预警排名
export const getDCGRedAlertRank = (data) => {
  return get(apiUrl.dcgRedAlertRank, data);
};

// 高频事项
export const getDCGHighFrequencyMatters = (data) => {
  return get(apiUrl.dcgHighFrequencyMatters, data);
};

// 红色预警明细
export const getDCGRedAlertDetails = (data) => {
  return get(apiUrl.dcgRedAlertDetails, data);
};

// 红黄色预警
export const getDCGRedYellowAlert = (data) => {
  return get(apiUrl.dcgRedYellowAlert, data);
};

// 工作办理
export const getDCGWorkTodo = (data) => {
  return get(apiUrl.dcgWorkTodo, data);
};

/**
 * 工作办理 纪委组
 * @param {*} type 1 代办 2已办
 * @returns
 */
export const getDcgWorkTodo4 = (data) => {
  return get(apiUrl.dcgWorkTodo4, data);
};

/**
 * 工作办理 部门
 * @param {*} type 1 代办 2已办
 * @returns
 */
export const getDeptWorkTodo = (data) => {
  return get(apiUrl.deptWorkTodo, data);
};

/**
 * 工作办理 部门 统计 
 * @returns
 */
export const getDeptTodoCount = (data) => {
  return get(apiUrl.deptTodoCount, data);
};
