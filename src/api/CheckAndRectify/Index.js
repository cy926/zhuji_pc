import { get, post } from "@/api/request";

const apiUrl = {
  list: "/checkMend/selectcheckMendList",
  feedbackList: "/checkMend/selectProcessList/",

  detail: "/checkMend/checkMendDetail", // 根据整改单ID查询详情

  delete: "/checkMend/deleteCheckMend", // 根据整改单ID删除

  add: "/checkMend/saveCheckMend", // 检查整改 新增

  feedbakAdd: "/checkMend/saveProcess", // 检查整改 新增反馈
  evaluation: "/checkMend/checkProcess", // 检查整改 反馈 评价
};

/**
 * 获取 检查整改列表
 * @param {*} data
 * @param {Number} pageNum
 * @param {Number} pageSize
 * @param {Number} assign_status 0 暂存 10 待反馈 20 待审核 30 已退回 100 处理完毕
 * @returns
 */
export const getCheckAndRectifyList = (data) => {
  return get(apiUrl.list, data);
};

/**
 * 获取 检查整改 反馈列表
 * @param {*} data
 * @param {Number} pageNum
 * @param {Number} pageSize
 * @param {Number} assign_status 0 暂存 10 待反馈 20 待审核 30 已退回 100 处理完毕
 * @returns
 */
export const getCheckAndRectifyFeedbackList = (data) => {
  return get(apiUrl.feedbackList, data);
};

/**
 * 获取 检查整改详情 By 整改单ID
 * @param {*} data
 * @param {Number} id 整改单ID
 * @returns
 */
export const getCheckAndRectifyDetails = (data) => {
  return get(apiUrl.detail, data);
};

/**
 * 删除 检查整改  By 整改单ID
 * @param {*} data
 * @param {Number} id 整改单ID
 * @returns
 */
export const deleteCheckAndRectify = (data) => {
  return get(apiUrl.delete, data);
};

/**
 * 新建整改单
 * @param {*} data { }
 * @returns
 */
export const addCheckAndRectify = (data) => {
  return post(apiUrl.add, data);
};

/**
 * 整改单 新增反馈
 * @param {*} data { }
 * @param {Number} check_mend_id 整改单ID
 * @param {Number} feedback_content 反馈内容
 * @returns
 */
export const addCheckAndRectifyFeedback = (data) => {
  return post(apiUrl.feedbakAdd, data);
};

/**
 * 整改单 / 反馈单 评价
 * @param {*} data { }
 * @param {Number} check_content 整改结果评价
 * @param {Number} id 反馈单ID
 * @param {Number} check_mend_id 整改单ID
 * @param {Number} check_status 审核状态 0 不通过 100 通过
 * @returns
 */
export const addCheckAndRectifyEvaluation = (data) => {
  return post(apiUrl.evaluation, data);
};
