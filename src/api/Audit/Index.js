import { get, post } from "@/api/request";

const apiUrl = {
  auditList: "/audit/auditList",
  auditDetail: "/audit/auditDetail",
  auditProjectList: "/audit/projectList",
  auditBatchAdd: "/audit/saveBatch",
  auditProjectAdd: "/audit/saveUpdate",
  auditRemove: "/audit/auditDetele",
};

export const getAuditList = (data) => {
  return get(apiUrl.auditList, data);
};

export const getAuditDetail = (data) => {
  return get(apiUrl.auditDetail, data);
};

export const getAuditProjectList = (data) => {
  return get(apiUrl.auditProjectList, data);
};

export const postAuditRemove = (data) => {
  return get(apiUrl.auditRemove, data);
};

export const postAuditBatchAdd = (data) => {
  return post(apiUrl.auditBatchAdd, data);
};

export const postAuditProjectAdd = (data) => {
  return post(apiUrl.auditProjectAdd, data);
};
