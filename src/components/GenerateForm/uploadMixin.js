import { postBindFiles } from "@/api/System/File";
export default {
  methods: {
    checkFile(id) {
      let files = this.$store.state.files.unBindFiles;
      if (files && files.length > 0) {
        this.bindFile(files, id);
      }
    },
    bindFile(filesArr, id) {
      return new Promise((resolve) => {
        if (id > 0) {
          if (filesArr.length > 0) {
            filesArr = filesArr.map((i) => {
              i.pid = id;
              return i;
            });

            postBindFiles({ files: JSON.stringify(filesArr) })
              .then((res) => {
                if (res.code == 0) {
                  this.resset();
                  resolve(true);
                } else {
                  resolve(false);
                }
              })
              .catch((err) => {
                resolve(false);
              });
          }
        }
      });
    },
    resset() {
      this.$store.commit("files/SET_CURRENT", 0);
      this.$store.commit("files/UNBIND_RESET", 0);
    },
  },
  beforeDestroy() {
    this.resset();
  },
  beforeRouteUpdate() {
    this.resset();
  },
};
