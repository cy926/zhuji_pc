import { commonSave, getSelectOptions } from "@/api/Index";
import uploadMixin from "./uploadMixin";
export default {
  mixins: [uploadMixin],
  data() {
    return {
      postData: {},
      creater: "",
      createrPhone: "",
    };
  },
  methods: {
    generateValue() {
      this.recursionValue(this.gData);
    },
    recursionValue(val) {
      return val.map((i) => {
        if (i.children) {
          this.recursionValue(i.children);
        } else {
          if (i.dbField) {
            this.postData[i.dbField] = i.bindModel;
          }
        }

        return i;
      });
    },
    /**
     * common save methods
     * @param {String} tableTarget
     * @param {int} id 0 add > 0 update
     */
    save(tableTarget, id) {
      if (typeof tableTarget != "string") {
        throw `tableTarget accept string get ${typeof tableTarget}`;
      }

      this.generateValue();

      this.postData["table_name"] = tableTarget;
      this.postData["id"] = id;

      this.postData["creater"] = this.creater;
      this.postData["createrPhone"] = this.createrPhone;

      if (this.checked) {
        commonSave(this.postData).then((res) => {
          if (res.code == 0) {
            this.checkFile(res.data);
            this.$message.success("保存成功");
            this.$router.go(-1);
          }
        });
      } else {
        this.$message.error("请勾选承诺并录入真实上报人信息");
      }
    },
    initOptions(target) {
      getSelectOptions({
        parameter: target.list_pointing,
        id: target.bindModel,
      }).then((res) => {
        if (res.code == 0) {
          this.$set(target, "options", res.data);
        }
      });
    },
  },
};
