export default {
  data() {
    return {
      questionOptions: [
        { label: "征地拆迁", value: "征地拆迁" },
        { label: "资金", value: "资金" },
        { label: "施工问题", value: "施工问题" },
        { label: "土地问题", value: "土地问题" },
        { label: "审批问题", value: "审批问题" },
        { label: "其他", value: "其他" },
      ],
    };
  },
};
