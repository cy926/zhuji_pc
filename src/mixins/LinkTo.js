export default {
  methods: {
    /**
     *
     * @param {string} where
     */
    linkTo(where) {
      if (typeof where != "string") {
        throw `where accept string got ${typeof where}`;
      }

      this.$router.push(where);
    },
  },
};
