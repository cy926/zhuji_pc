export default {
  methods: {
    initHeight() {
      let boxDom = document.getElementsByClassName("full-box")[0];

      let operateDom = document.getElementsByClassName("operate-bar")[0];

      let tableDom = document.getElementsByClassName("base-table")[0];

      console.log(boxDom.clientHeight);
      console.log(operateDom.clientHeight);
      console.log(tableDom.clientHeight);

      tableDom.style.height = boxDom.clientHeight - operateDom.clientHeight;
    },
  },
  mounted() {
    this.initHeight();
  },
};
