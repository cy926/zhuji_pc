import { getIndustry } from "@/api/YearPlan/Index";

export default {
  data() {
    return {
      industryOptions: [],
    };
  },
  methods: {
    initOptions() {
      getIndustry().then((res) => {
        if (res.code == 0) {
          this.industryOptions = [
            {
              dict_value: "全部",
              value: "",
            },
            ...res.data,
          ];
        }
      });
    },
  },
  created() {
    this.queryParams.plan_year = new Date().getFullYear() + 1 + "";
    this.initOptions();
  },
};
