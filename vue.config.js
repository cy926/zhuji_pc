const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: true,
  chainWebpack: (config) => {
    config.plugin("html").tap((args) => {
      args[0].title = "公共投资项目全生命周期监管系统";
      return args;
    });
  },
  css: {
    loaderOptions: {
      scss: {
        additionalData: `@import "@/assets/globalVariable.scss";`,
      },
    },
  },
  lintOnSave: false,
  devServer: {
    port: 12666,
    https: false,
    proxy: {
      "/api": {
        target: process.env.VUE_APP_BASE_URL,
        changeOrigin: true, // 是否改变域名
        logLevel: "debug",
        pathRewrite: {
          // 路径重写
          "^/api": "",
        },
      },
    },
  },
  productionSourceMap: false,
});
